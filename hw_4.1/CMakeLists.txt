cmake_minimum_required(VERSION 3.16.0)
project(render-basic CXX)

add_executable(render-basic canvas_basic.cxx
                           canvas_basic.hxx
                           canvas_basic_main.cxx
                           )
target_compile_features(render-basic PUBLIC cxx_std_17)

add_executable(render-line line_render.cxx
                           line_render.hxx
                           line_render_main.cxx
                           canvas_basic.cxx
                           )
target_compile_features(render-line PUBLIC cxx_std_17)

add_executable(render-triangle triangle_render.cxx
                               triangle_render.hxx
                               triangle_render_main.cxx
                               canvas_basic.cxx
                               line_render.cxx
                           )
target_compile_features(render-triangle PUBLIC cxx_std_17)

add_executable(render-triangle-indexed triangle_indexed_render.cxx
                                       triangle_indexed_render.hxx
                                       triangle_indexed_render_main.cxx
                                       canvas_basic.cxx
                                       line_render.cxx
                                       triangle_render.cxx
                           )
target_compile_features(render-triangle-indexed PUBLIC cxx_std_17)

add_executable(triangle-interpolated  triangle_interpolated_render.cxx
                                      triangle_interpolated_render.hxx
                                      triangle_interpolated_render_main.cxx
                                      canvas_basic.cxx
                                      line_render.cxx
                                      triangle_render.cxx
                                      triangle_indexed_render.cxx
                           )
target_compile_features(triangle-interpolated PUBLIC cxx_std_17)

install(TARGETS render-basic
        RUNTIME DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        LIBRARY DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        ARCHIVE DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests)
